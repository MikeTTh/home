{ ... }:
{
  imports = [
    ./system.nix
    ./home-manager.nix
    ./settings.nix
    ./android-integration.nix
  ];
}
