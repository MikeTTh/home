{ inputs
, unstable
, flake
, ...
}:
{
  home-manager = {
    extraSpecialArgs = {
      inherit inputs unstable flake;
      osImport = ./../home-manager/os/android;
      userName = "nix-on-droid";
      homeDir = "/data/data/com.termux.nix/files/home";
    };
    config = ./../home-manager;
  };
}
