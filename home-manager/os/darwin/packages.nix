{ pkgs
, unstable
, flake
, ...
}:
let
  awx = flake.packages."${pkgs.system}".awx;
in
{
  home.packages = with pkgs; [
    berglas
    coreutils
    bazelisk
    buildifier
    unstable.bitrise
    awx
    unstable.ollama
    unstable.air
    opentofu
    (google-cloud-sdk.withExtraComponents [
      google-cloud-sdk.components.gke-gcloud-auth-plugin
      google-cloud-sdk.components.pubsub-emulator
    ])
  ];
}
