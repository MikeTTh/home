{ pkgs, unstable, ... }:
{
  home.packages = with pkgs; [
    # tools
    psmisc
    usbutils
    woeusb-ng
    ventoy-full
    docker-credential-helpers
    sshfs
    progress
    _7zz
    pv
    ollama-rocm
    unstable.gcc

    # everyday life
    pbpctrl
    unrar
    ddcutil
  ];
}
