{ pkgs
, lib
, ...
}:
{
  services.mpris-proxy.enable = true;

  systemd.user.services.clean-downloads =
    let
      find = lib.getExe pkgs.findutils;
      cleaner-script = pkgs.writeText "clean-downloads.sh" ''
        #!/bin/sh

        set -e

        ${find} ~/Downloads -type f -mtime +30 -delete
        ${find} ~/Downloads -type d -empty -delete
      '';
    in
    {
      Unit = {
        Description = "Clean Downloads";
      };

      Service = {
        ExecStart = "/bin/sh ${cleaner-script}";
      };
    };

  systemd.user.timers.clean-downloads = {
    Unit.Description = "Clean Downloads";
    Install.WantedBy = [ "timers.target" ];
    Timer = {
      OnCalendar = "weekly";
      Persistent = true;
    };
  };

  systemd.user.services.goldwarden = {
    Install.WantedBy = [ "graphical-session.target" ];

    Unit = {
      Description = "Goldwarden";
    };

    Service = {
      ExecStart = "${pkgs.goldwarden}/bin/goldwarden daemonize";
    };
  };

  services.ssh-agent.enable = true;
  systemd.user.sessionVariables = {
    SSH_ASKPASS = lib.getExe pkgs.kdePackages.ksshaskpass;
    SSH_ASKPASS_REQUIRE = "prefer";
    SSH_AUTH_SOCK = "$XDG_RUNTIME_DIR/ssh-agent";
  };
}
