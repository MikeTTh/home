{ pkgs, ... }:
let
  customCompletions = pkgs.runCommand "customCompletions" { } ''
    mkdir -p $out/share/zsh/site-functions
    ${pkgs.goldwarden}/bin/goldwarden completion zsh > $out/share/zsh/site-functions/_goldwarden
  '';
in
{
  home.packages = [ customCompletions ];

  systemd.user.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };

  xdg = {
    enable = true;
    mime.enable = true;
  };

  dconf = {
    enable = true;
    settings = {
      "org/virt-manager/virt-manager/confirm" = {
        delete-storage = true;
        unapplied-dev = true;
        forcepoweroff = false;
      };
      "org/virt-manager/virt-manager/connections" = {
        autoconnect = [ "qemu:///system" ];
        uris = [ "qemu:///system" ];
      };
      "org/virt-manager/virt-manager/console".auto-redirect = false;
      "org/virt-manager/virt-manager/details".show-toolbar = true;
      "org/virt-manager/virt-manager/paths" = {
        image-default = "/home/mike/Downloads";
        media-default = "/home/mike/Downloads";
      };
      "org/virt-manager/virt-manager".xmleditor-enabled = true;

      "io/github/amit9838/mousam" = {
        added-cities = [ "Budapest,Hungary,47.49835,19.04045" ];
        selected-city = "47.49835,19.04045";
      };

      "io/missioncenter/MissionCenter" = {
        apps-page-merged-process-stats = true;
        apps-page-remember-sorting = true;
        performance-page-cpu-graph = 2;
      };
    };
  };
}
