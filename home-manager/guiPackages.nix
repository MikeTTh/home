{ config
, pkgs
, lib
, unstable
, ...
}:
{
  config = lib.mkIf config.hasGUI {
    home.packages = with pkgs; [
      remmina
      unstable.gimp
      unstable.inkscape-with-extensions
      audacity
      unstable.virt-manager
      bruno
      localsend
    ];
  };

  options.hasGUI =
    with lib;
    mkOption {
      type = types.bool;
      default = true;
      defaultText = literalExpression "true";
      description = "Install GUI packages";
      example = literalExpression "false";
    };
}
