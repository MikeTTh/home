-- see https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md for possible options
-- don't forget to add the LSP server to programs.neovim.extraPackages

local lspconfig = require('lspconfig')

-- Go
lspconfig.gopls.setup {}
lspconfig.golangci_lint_ls.setup {}

-- Markdown
lspconfig.marksman.setup {}
