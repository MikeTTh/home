{ pkgs, unstable, ... }:
{
  home.packages = with pkgs; [
    # basic utils
    neofetch
    openssh
    zip
    tree
    lsof
    pigz
    unixtools.xxd
    curlHTTP3
    grpcurl
    dig
    nmap
    jq
    yq-go
    sshpass
    pwgen
    file
    unzip
    binwalk
    tun2socks
    bat

    # dev land
    buf
    screen
    git
    git-lfs
    nodejs
    gnumake
    unstable.bun
    unstable.deno
    pkg-config
    unstable.golangci-lint
    kubectl
    kubernetes-helm
    python3
    uv
    jdk
    graphviz
    picocom
    ipcalc
    nix-output-monitor
    attic-client
    android-tools
    kubelogin-oidc

    # fun stuff
    cowsay
    fortune
    lolcat
    figlet
    cmatrix
  ];

  nixpkgs.config.allowUnfree = true;
}
