{ unstable, ... }:
{
  imports = [
    ./git.nix
    ./zsh.nix
    ./nvim.nix
    ./tmux.nix
    ./ssh.nix
    ./term.nix
    ./zed.nix
    ./nix.nix
    ./ansible-mitogen.nix
  ];

  home.sessionVariables = {
    EDITOR = "nvim";
  };

  home.file = {
    ".alsoftrc".text = ''
      [general]
      drivers = pipewire,pulse,jack,alsa,
      [pulse]
      allow-moves = true
      fix-rate = true
    '';
  };

  programs.go = {
    enable = true;
    goBin = ".local/share/go/bin";
    goPath = ".local/share/go";
    package = unstable.go_1_23;
  };
}
