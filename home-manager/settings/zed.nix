{ lib
, config
, unstable
, ...
}:
{
  config = lib.mkIf config.hasGUI {
    home.packages = [ unstable.zed-editor ];

    xdg.configFile."zed/settings.json".text = builtins.toJSON {
      theme = "Ayu Dark";
      ui_font_size = 16;
      buffer_fonts_size = 16;
      buffer_font_family = "FiraCode Nerd Font";
      autosave = "on_focus_change";
      tabs = {
        git_status = true;
        file_icons = true;
      };
      hour_format = "hour24";
      project_panel = {
        git_status = true;
      };
    };
  };
}
