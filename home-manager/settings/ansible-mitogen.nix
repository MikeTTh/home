{ pkgs, ... }:
let
  mitogen = pkgs.fetchzip {
    url = "https://github.com/mitogen-hq/mitogen/releases/download/v0.3.22/mitogen-0.3.22.tar.gz";
    hash = "sha256-DIs7aS1oeWA/VDzBS4Pcnjq7TYltJ0qs3Iyzxcet+78=";
  };
in
{
  home.sessionVariables = {
    ANSIBLE_STRATEGY_PLUGINS = "${mitogen}/ansible_mitogen/plugins/strategy";
    no_proxy = "*";
  };
}
