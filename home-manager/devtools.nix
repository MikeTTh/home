{ config, ... }:
{
  home.file = {
    ".local/toolchains/go".source = config.programs.go.package;
  };
  home.sessionVariables = {
    "GOROOT" = "${config.home.homeDirectory}/.local/toolchains/go/share/go";
  };
}
