self: rec {
  forEachSystem = (
    systems: func:
      builtins.foldl'
        (
          acc: system:
            let
              pkgs = self.inputs.nixpkgs.legacyPackages."${system}";
            in
            { "${system}" = func { inherit system pkgs; }; } // acc
        )
        { }
        systems
  );

  linuxSystems = [
    "x86_64-linux"
    "aarch64-linux"
  ];
  forEachLinux = forEachSystem linuxSystems;

  darwinSystems = [
    "x86_64-darwin"
    "aarch64-darwin"
  ];

  defaultSystems = linuxSystems ++ darwinSystems;
  forEachDefaultSystem = forEachSystem defaultSystems;

  pkgsForSystem = (
    input: system: {
      inherit system;
      pkgs = input.legacyPackages."${system}";
    }
  );

  forEachDefaultPkgs = (func: forEachSystem (system: func (pkgsForSystem system)));
}
