check: fmt lint

lint:
	for file in $$(find . -name "*.nix"); do \
		echo $$file; nix run unstable#nixf < $$file; \
	done

fmt:
	nix fmt

push-cache-inputs:
	echo "pushing inputs"
	nix flake archive --json | jq -r '.path,(.inputs|to_entries[].value.path)' | xargs attic push default
push-cache-linux:
	echo "pushing optimism"
	attic push default $$(nix build --no-link --print-out-paths ".#nixosConfigurations.optimism.config.system.build.toplevel")
push-cache-macos:
	echo "pushing miklostoth-mbp"
	attic push default $$(nix build --no-link --print-out-paths ".#darwinConfigurations.miklostoth-mbp.system")

OS_NAME := $(shell uname -s)
push-cache:
	$(MAKE) push-cache-inputs
ifeq ($(OS_NAME),Linux)
	$(MAKE) push-cache-linux
endif
ifeq ($(OS_NAME),Darwin)
	$(MAKE) push-cache-macos
endif

