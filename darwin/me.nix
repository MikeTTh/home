{ ... }:
{
  users.users."miklos.toth" = {
    description = "Miklós Tóth";
    home = "/Users/miklos.toth";
  };

  # staff is Darwin's wheel
  nix.settings.trusted-users = [
    "@staff"
    "miklos.toth"
  ];
}
