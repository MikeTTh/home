{ inputs
, unstable
, flake
, ...
}:
{
  imports = [ inputs.home-manager.darwinModules.home-manager ];

  home-manager = {
    extraSpecialArgs = {
      inherit inputs unstable flake;
      osImport = ./../home-manager/os/darwin;
      userName = "miklos.toth";
      homeDir = "/Users/miklos.toth";
    };
    users."miklos.toth" = import ./../home-manager;
  };
}
