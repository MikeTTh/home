{ inputs, ... }:
{
  imports = [ inputs.nix-homebrew.darwinModules.nix-homebrew ];
  nix-homebrew = {
    enable = true;
    user = "miklos.toth";
    taps = {
      "homebrew/homebrew-core" = inputs.homebrew-core;
      "homebrew/homebrew-cask" = inputs.homebrew-cask;
      "homebrew/homebrew-bundle" = inputs.homebrew-bundle;
    };
    mutableTaps = false;
  };

  homebrew = {
    enable = true;
    taps = [
      "homebrew/core"
      "homebrew/cask"
      "homebrew/bundle"
    ];
    brews = [
      "cliclick"
      "blueutil"
    ];
    casks = [
      "bruno"
      "alt-tab"
      "macfuse"
      "obs"
      "vlc"
      "utm"
      "monitorcontrol"
      "jordanbaird-ice"
      "deskpad"
      "ghostty"
      "docker"
      "easy-move+resize"
      "xquartz"
      "jetbrains-toolbox"
      "karabiner-elements"
      "unnaturalscrollwheels"
      "middleclick"
      "firefox"
      "arc"
      "brave-browser"
      "zen-browser"
      "zed"
      "visual-studio-code"
      "wireshark"
      "vmware-fusion"
      "tailscale"
      "telegram"
      "signal"
      "notunes"
      "tigervnc-viewer"
      "qflipper"
      "tidal"
      "supertuxkart"
      "zoom"
      "whatsyoursign"
    ];
    masApps = {
      Xcode = 497799835;
      Velja = 1607635845;
      Keynote = 409183694;
      Pages = 409201541;
      Numbers = 409203825;
      "DaVinci Resolve" = 571213070;
      "Apple Configurator" = 1037126344;
    };
    onActivation = {
      cleanup = "uninstall";
      upgrade = true;
    };
  };
}
