{ lib
, fetchFromGitHub
, stdenvNoCC
}:
stdenvNoCC.mkDerivation rec {
  pname = "shantell-sans";
  version = "1.011";

  phases = [
    "unpackPhase"
    "installPhase"
  ];

  src = fetchFromGitHub {
    owner = "arrowtype";
    repo = "shantell-sans";
    rev = version;
    hash = "sha256-eF1s4tD/L36Q8gZW2CabDpHHYxF5l29UT64eNF1qzT8=";
  };

  configurePhase = "";
  buildPhase = "";

  installPhase = ''
    local out_font=$out/share/fonts/shantell-sans
    install -m444 -Dt $out_font fonts/Shantell\ Sans/Desktop/Static/TTF/*.ttf
    install -m444 -Dt $out_font fonts/Shantell\ Sans/Desktop/*.ttf
  '';

  meta = {
    description = "Beautiful Comic Sans alternative";
    homepage = "https://shantellsans.com/";
    license = lib.licenses.ofl;
    platforms = lib.platforms.all;
  };
}
