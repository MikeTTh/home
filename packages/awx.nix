{ fetchPypi, python3Packages }:
python3Packages.buildPythonPackage rec {
  pname = "awxkit";
  version = "23.6.0";
  format = "wheel";
  src = fetchPypi {
    inherit pname version format;
    python = "py3";
    dist = "py3";
    sha256 = "sha256-Pq6DN5d1uMZKYE0XSFZOuG/TyZFriQ4IKF8e2u+8Ikc=";
  };
  doCheck = false;
  propagatedBuildInputs = with python3Packages; [
    requests
    pyyaml
    setuptools
  ];
  meta.mainProgram = "awx";
}
