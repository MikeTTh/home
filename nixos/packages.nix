{ pkgs, flake, ... }:
{
  nixpkgs.config.allowUnfree = true;

  nix = {
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      auto-optimise-store = true;
      substituters = flake.nixConfig.extra-substituters;
      trusted-public-keys = flake.nixConfig.extra-trusted-public-keys;
    };
    package = pkgs.lix;
  };

  environment.systemPackages = with pkgs; [
    gamescope-wsi
    zsh
    kitty
    wl-clipboard
    htop
    lm_sensors
    virt-manager
    tpm2-tss
    nixos-option
    sysstat
    qflipper
    pciutils
  ];

  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    withNodeJs = true;
    withPython3 = true;
  };

  programs.adb.enable = true;

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    gamescopeSession = {
      enable = true;
      args = [
        "--adaptive-sync"
        "--rt"
      ];
    };
  };

  programs.gamescope = {
    enable = true;
    capSysNice = true;
    args = [
      "--adaptive-sync"
      "--rt"
    ];
  };

  programs.mtr.enable = true;
  programs.wireshark = {
    enable = true;
    package = pkgs.wireshark-qt;
  };

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  environment.shells = with pkgs; [ zsh ];

  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-extra
  ];
}
