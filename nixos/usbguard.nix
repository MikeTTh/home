{ pkgs, ... }:
{
  services.usbguard = {
    enable = true;
    dbus.enable = true;
    IPCAllowedUsers = [
      "root"
      "mike"
    ];
    IPCAllowedGroups = [ "wheel" ];
    insertedDevicePolicy = "apply-policy";
    presentControllerPolicy = "apply-policy";
    presentDevicePolicy = "apply-policy";
    implicitPolicyTarget = "block";
    rules = ''
      allow with-connect-type ""
      allow with-connect-type "hardwired"
      allow id 32ac:0002 name "HDMI Expansion Card" hash "WG97W3Y1SAl8mcsg7mFZY4GzAPAKNEJPOg+AOJ18sgo="
      allow id 0bda:8156 name "USB 10/100/1G/2.5G LAN" hash "nlPmS7VnOmW4llwz7MzKzc/oOF5SE9fbDj//SaDVXzk="

      # Philips monitor
      allow id 2109:2817 serial "000000000" name "USB2.0 Hub             " hash "Bsyx+7MYFKTtm5UPO6Xw6x/v7RIQXvo4xZ/KK43ODUQ="
      allow id 2109:0817 serial "000000000" name "USB3.0 Hub             " hash "DVu1KVbgXAJWl7EBXzs4lInLSj1OFOJZdxEprgm0RKQ="
      allow id 2109:2211 serial "" name "USB2.0 Hub             " hash "68wuF0VjSGGjqXIiEu0WAc3iQpySHnvo8qXimFvMdUg="
      allow id 2109:0211 serial "" name "USB3.0 Hub             " hash "u+TwAJvp/s7uKUbofN7FsYyhvcXUg7gNsK5Ta6MJ7rU="
      allow id 2109:8884 serial "0000000000000001" name "USB Billboard Device   " hash "K6nmwK3YOjgPg7nrk4+403lSCBF9NHk9kWI7vjus1lA="
      allow id 0cf2:b201 serial "6243168002" name "PHL49M2C8900" hash "A0sArE+c67B6y7dm7cRjNTciZzJtCPV+pN7B4oG3DNc="

      allow id 17ef:482f serial "01.00.00" name "Lenovo 500 RGB Camera" hash "xGXmVwh5VYyA2a3WIHMu1qqOvnJiUR0L9nilPhj7CSU="
      allow id 3434:0123 serial "" name "Keychron Q3" hash "RYbj8Ream+PE2Eucg0Zqf4Ardp4K/mnquA6Qrg6/aKs="
      allow id 046d:c52b serial "" name "USB Receiver" hash "djeL7wNsJBQMuBiqUyWflgupndhsbPkbOih8g3L6OeA="

    '';
  };

  systemd.user.services."usbguard-notifier" = {
    enable = true;
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.usbguard-notifier}/bin/usbguard-notifier";
      Restart = "always";
    };
    wantedBy = [ "graphical-session.target" ];
  };
}
