{ inputs
, unstable
, flake
, ...
}:
{
  imports = [ inputs.home-manager.nixosModules.default ];

  home-manager = {
    extraSpecialArgs = {
      inherit inputs unstable flake;
      osImport = ./../home-manager/os/linux;
      userName = "mike";
      homeDir = "/home/mike";
    };
    users."mike" = import ./../home-manager;
  };
}
