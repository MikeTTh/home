{ ... }:
{
  users.users.mike = {
    isNormalUser = true;
    description = "Miklós Tóth";
    extraGroups = [
      "networkmanager"
      "wheel"
      "libvirtd"
      "input"
      "wireshark"
      "adbusers"
      "rtkit"
      "dialout"
      "video"
      "ydotool"
    ];
  };

  nix.settings.trusted-users = [ "@wheel" ];
}
