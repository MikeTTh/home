{ ... }:
{
  boot.binfmt = {
    emulatedSystems = [
      "armv7l-linux"
      "aarch64-linux"
      "riscv64-linux"
      "wasm64-wasi"
    ];
    preferStaticEmulators = true;
  };
}
