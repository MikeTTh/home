{ lib, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot = {
    initrd = {
      availableKernelModules = [
        "nvme"
        "xhci_pci"
        "thunderbolt"
        "usbhid"
        "amdgpu"
        "aesni_intel"
        "cryptd"
      ];
      kernelModules = [
        "thunderbolt"
        "aesni_intel"
        "xhci_pci"
        "usbhid"
        "amdgpu"
      ];
      luks.devices = {
        "luks-fe3b3c11-0b38-47eb-b9cf-a0a85cc22a24" = {
          device = "/dev/disk/by-uuid/fe3b3c11-0b38-47eb-b9cf-a0a85cc22a24";
          allowDiscards = true;
          bypassWorkqueues = true;
        };
        "luks-f4c816f0-2c2f-41a0-b5c0-3aa28b02b3fd" = {
          device = "/dev/disk/by-uuid/f4c816f0-2c2f-41a0-b5c0-3aa28b02b3fd";
          allowDiscards = true;
          bypassWorkqueues = true;
        };
      };
    };
    kernelModules = [ "kvm-amd" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/538382f9-ecf5-4add-92a1-0aa28e5f4b73";
      fsType = "ext4";
      options = [
        "noatime"
        "nodiratime"
        "discard"
      ];
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/4BEA-9E55";
      fsType = "vfat";
    };
  };

  swapDevices = [{ device = "/dev/disk/by-uuid/1128e7c3-1c54-4810-90e7-d622a22fa50b"; }];
  boot.resumeDevice = "/dev/disk/by-uuid/1128e7c3-1c54-4810-90e7-d622a22fa50b";

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.amd.updateMicrocode = true;
}
