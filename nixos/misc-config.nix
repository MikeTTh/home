{ pkgs, ... }:
{
  environment.sessionVariables = {
    NIXOS_OZONE_WL = "1";
  };

  # make sudo safe
  security.sudo-rs = {
    enable = true;
  };
}
