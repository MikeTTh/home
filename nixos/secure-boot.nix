{ pkgs
, lib
, inputs
, ...
}:
{
  # See Lanazboote guide: https://github.com/nix-community/lanzaboote/blob/master/docs/QUICK_START.md
  imports = [ inputs.lanzaboote.nixosModules.lanzaboote ];

  environment.systemPackages = with pkgs; [ sbctl ];

  # Lanzaboote replaces systemd-boot with a shimmed version
  boot.loader.systemd-boot.enable = lib.mkForce false;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.lanzaboote = {
    enable = true;
    # generated using `sudo sbctl create-keys`
    pkiBundle = "/etc/secureboot";
  };
}
